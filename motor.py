import sys
import RPi.GPIO as GPIO
from time import sleep

IN1 = 18
IN2 = 17
IN3 = 27
IN4 = 22

EN1 = 23
EN2 = 24

# GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()

# ENable pins
GPIO.setup(EN1, GPIO.OUT)
GPIO.setup(EN2, GPIO.OUT)

MotorR = GPIO.PWM(EN1, 50)
MotorL = GPIO.PWM(EN2, 50)

MotorR.start(0)
MotorL.start(0)

# MotorR
GPIO.setup(IN1, GPIO.OUT)
GPIO.setup(IN2, GPIO.OUT)

# MotorL
GPIO.setup(IN4, GPIO.OUT)
GPIO.setup(IN3, GPIO.OUT)


def MotorR_forward():
    GPIO.output(IN1, GPIO.HIGH)
    GPIO.output(IN2, GPIO.LOW)


def MotorL_forward():
    GPIO.output(IN4, GPIO.HIGH)
    GPIO.output(IN3, GPIO.LOW)


def MotorR_backward():
    GPIO.output(IN1, GPIO.LOW)
    GPIO.output(IN2, GPIO.HIGH)


def MotorL_backward():
    GPIO.output(IN4, GPIO.LOW)
    GPIO.output(IN3, GPIO.HIGH)


def MotorR_stop():
    GPIO.output(IN1, GPIO.LOW)
    GPIO.output(IN2, GPIO.LOW)


def MotorL_stop():
    GPIO.output(IN4, GPIO.LOW)
    GPIO.output(IN3, GPIO.LOW)


def forward_right(speed):
    print "doing '{0}' with speed: {1}".format(sys._getframe().f_code.co_name, speed)
    MotorR.ChangeDutyCycle(speed * .65)
    MotorL.ChangeDutyCycle(speed)
    MotorR_forward()
    MotorL_forward()


def forward(speed):
    print "doing '{0}' with speed: {1}".format(sys._getframe().f_code.co_name, speed)
    MotorR.ChangeDutyCycle(speed)
    MotorL.ChangeDutyCycle(speed)
    MotorR_forward()
    MotorL_forward()


def forward_left(speed):
    print "doing '{0}' with speed: {1}".format(sys._getframe().f_code.co_name, speed)
    MotorR.ChangeDutyCycle(speed)
    MotorL.ChangeDutyCycle(speed * .65)
    MotorR_forward()
    MotorL_forward()


def right(speed):
    print "doing '{0}' with speed: {1}".format(sys._getframe().f_code.co_name, speed)
    MotorR.ChangeDutyCycle(speed)
    MotorL.ChangeDutyCycle(0)
    MotorL_forward()


def pivot_right(speed):
    print "doing '{0}' with speed: {1}".format(sys._getframe().f_code.co_name, speed)
    MotorR.ChangeDutyCycle(speed)
    MotorL.ChangeDutyCycle(speed)
    MotorR_backward()
    MotorL_forward()


def stop(speed):
    print "doing '{0}' with speed: {1}".format(sys._getframe().f_code.co_name, 0)
    MotorL.ChangeDutyCycle(0)
    MotorR.ChangeDutyCycle(0)
    MotorR_stop()
    MotorL_stop()


def left(speed):
    print "doing '{0}' with speed: {1}".format(sys._getframe().f_code.co_name, speed)
    MotorR.ChangeDutyCycle(0)
    MotorL.ChangeDutyCycle(speed)
    MotorR_forward()


def pivot_left(speed):
    print "doing '{0}' with speed: {1}".format(sys._getframe().f_code.co_name, speed)
    MotorR.ChangeDutyCycle(speed)
    MotorL.ChangeDutyCycle(speed)
    MotorR_forward()
    MotorL_backward()


def backward_right(speed):
    print "doing '{0}' with speed: {1}".format(sys._getframe().f_code.co_name, speed)
    MotorR.ChangeDutyCycle(speed * .65)
    MotorL.ChangeDutyCycle(speed)
    MotorR_backward()
    MotorL_backward()


def backward(speed):
    print "doing '{0}' with speed: {1}".format(sys._getframe().f_code.co_name, speed)
    MotorR.ChangeDutyCycle(speed)
    MotorL.ChangeDutyCycle(speed)
    MotorR_backward()
    MotorL_backward()


def backward_left(speed):
    print "doing '{0}' with speed: {1}".format(sys._getframe().f_code.co_name, speed)
    MotorR.ChangeDutyCycle(speed)
    MotorL.ChangeDutyCycle(speed * .65)
    MotorR_backward()
    MotorL_backward()


def loop():
    # forward(90)
    # sleep(5)
    # backward(80)
    # sleep(10)
    # stop(0)
    # sleep(1)
    # left(75)
    # sleep(1)
    # right(75)
    # sleep(1)
    # stop(0)
    # pivot_left(100)
    # sleep(2)
    # pivot_right(100)
    # sleep(2)
    exit_gracefully()

# count = 50
# while (count <= 100):
#    forward(count)
#    sleep(0.5)
#    stop(0)
#    count = count + 2

# try:
# GPIO.output(IN1, GPIO.HIGH)
# GPIO.output(IN2, GPIO.LOW)
#     GPIO.output(IN4, GPIO.HIGH)
#     GPIO.output(IN3, GPIO.LOW)
#     while 1:
#         print "first"
#         for dc in range(0, 101, 5):
#             MotorL.ChangeDutyCycle(dc)
#             sleep(0.1)

#         print "2nd"
#         for dc in range(100, -1, -5):
#             MotorL.ChangeDutyCycle(dc)
#             sleep(0.1)

# except KeyboardInterrupt:
#     pass


def exit_gracefully():
    MotorL.stop()
    MotorR.stop()
    GPIO.cleanup()
    sys.exit(0)

options = {
    9: forward_right,
    8: forward,
    7: forward_left,
    6: pivot_right,
    5: stop,
    4: pivot_left,
    3: backward_right,
    2: backward,
    1: backward_left
}


class _GetchUnix:

    def __init__(self):
        import tty
        import sys

    def __call__(self):
        import sys
        import tty
        import termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch


getch = _GetchUnix()

try:
    while True:
        try:
            mode = int(getch())
            options[mode](90)

        except ValueError:
            print "Not a number -> terminating"
            exit_gracefully()


except KeyboardInterrupt:
    print "Caught KeyboardInterrupt -> terminating"
    exit_gracefully()
